# Linux-Tage 2023
A small demo for Kotlin multiplatform. The code is organized as follows:
* `common`: model code (conference, speaker, etc.) and a static repository
  * `compose-common`: [Compose-Multiplatform](https://github.com/JetBrains/compose-multiplatform) 
      UI code, only a frame (window, Fragment, etc.) is missing
    * `android`: show Compose UI on Android phone/simulator
    * `./gradlew :desktop:run`: show Compose UI on desktop (Linux, Mac, Win)
    * `./gradlew :web:jsBrowserDevelopmentRun`: show Compose UI in web browser, canvas rendering (no DOM), alpha-version!
  * `ios`: XCode project for SwiftUI implementation

## Possible Enhancements
* static repository -> web server (e.g., [ktor](https://ktor.io)) with a DB
  * make clients reactive (e.g., with Flows) to display changed information in clients
* use [Kotlin React](https://kotlinlang.org/docs/js-get-started.html) for another web client that uses the DOM
