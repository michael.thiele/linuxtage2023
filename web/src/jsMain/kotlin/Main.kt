import androidx.compose.ui.window.Window
import de.connect2x.common.App
import org.jetbrains.skiko.wasm.onWasmReady


fun main() {
    onWasmReady {
        Window("Linux-Tage 2023") {
            App()
        }
    }
}