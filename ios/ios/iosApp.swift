//
//  iosApp.swift
//  ios
//
//  Created by Michael Thiele on 10.03.23.
//

import SwiftUI

@main
struct iosApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
