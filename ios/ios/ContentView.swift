//
//  ContentView.swift
//  ios
//
//  Created by Michael Thiele on 10.03.23.
//

import SwiftUI
import common

struct ContentView: View {
    let conference = DataSource.shared.linuxTage2023
    
    var body: some View {
        VStack {
            Text(conference.name)
                .font(.title)
            Text(conference.location)
                .font(.title2)
                .padding(.bottom, 20)
            ConferenceDays(conference)
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
