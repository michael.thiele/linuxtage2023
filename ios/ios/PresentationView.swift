//
//  PresentationView.swift
//  ios
//
//  Created by Michael Thiele on 10.03.23.
//

import SwiftUI
import common

struct PresentationView: View {
    let presentation: Presentation
    @State var isFavorite: Bool
    
    init(_ presentation: Presentation) {
        self.presentation = presentation
        self.isFavorite = presentation.favorite
    }
    
    var body: some View {
        let begin = presentation.time.formatTime()
        let end = presentation.until().formatTime()
        let duration = presentation.durationInMinutes
        
        VStack(alignment: .leading) {
            Text("\(begin) - \(end) (\(duration) Minuten)")
                .font(.caption)
            HStack {
                Toggle(isOn: $isFavorite, label: {
                    Label(title: {
                        Text(presentation.title)
                            .fontWeight(.bold)
                            .minimumScaleFactor(0.9)
                            .lineLimit(3)
                    }, icon: {
                        EmptyView()
                    })
                })
            }
            Text(presentation.speaker.map{$0.fullName}.joined(separator: ", "))
        }
    }
}

struct PresentationView_Previews: PreviewProvider {
    static var previews: some View {
        PresentationView(DataSource.shared.linuxTage2023.presentations[0])
    }
}
