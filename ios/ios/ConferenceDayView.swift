//
//  ConferenceDay.swift
//  ios
//
//  Created by Michael Thiele on 10.03.23.
//

import SwiftUI
import common

struct ConferenceDayView: View {
    let conferenceDay: Kotlinx_datetimeLocalDate
    let presentations: Array<Presentation>
    
    init(_ conferenceDay: Kotlinx_datetimeLocalDate, _ presentations: Array<Presentation>) {
        self.conferenceDay = conferenceDay
        self.presentations = presentations
    }
    
    var body: some View {
        VStack {
            Text(conferenceDay.format())
                .font(.title3)
            List(presentations, id: \.self.title) { presentation in
                PresentationView(presentation)
            }
        }
    }
}

struct ConferenceDay_Previews: PreviewProvider {
    static var previews: some View {
        ConferenceDayView(DataSource.shared.linuxTage2023.conferenceDays[0], DataSource.shared.linuxTage2023.presentations)
    }
}
