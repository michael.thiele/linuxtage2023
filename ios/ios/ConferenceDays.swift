//
//  ConferenceDays.swift
//  ios
//
//  Created by Michael Thiele on 10.03.23.
//

import SwiftUI
import common

struct ConferenceDays: View {
    let conference: Conference
    
    init(_ conference: Conference) {
        self.conference = conference
    }
    
    var body: some View {
        TabView {
            ForEach(conference.conferenceDays, id: \.self) { conferenceDay in
                ConferenceDayView(conferenceDay, conference.presentationsOf(day: conferenceDay))
            }
        }
        .tabViewStyle(.page)
        .indexViewStyle(.page(backgroundDisplayMode: .always))
    }
}

struct ConferenceDays_Previews: PreviewProvider {
    static var previews: some View {
        ConferenceDays(DataSource.shared.linuxTage2023)
    }
}
