package de.connect2x.common.conference

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import de.connect2x.linuxtage2023.model.Presentation

@Composable
fun ConferenceDay(presentations: List<Presentation>) {
    val scrollState = rememberScrollState()

    Column(
        Modifier
            .padding(top = 20.dp)
            .verticalScroll(scrollState)
    ) {
        presentations.map { presentation ->
            Presentation(presentation)
            Divider(Modifier.padding(20.dp))
        }
    }
}