package de.connect2x.common.conference

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import de.connect2x.linuxtage2023.model.Conference
import de.connect2x.linuxtage2023.model.util.format

@Composable
fun ConferenceDays(conference: Conference) {
    val selectedDay = remember { mutableStateOf(0) }
    val conferenceDays = conference.conferenceDays
    val conferenceDay = conferenceDays[selectedDay.value]

    TabRow(selectedDay.value) {
        List(conferenceDays.size) { day ->
            Tab(
                selected = selectedDay.value == day,
                onClick = { selectedDay.value = day },
            ) {
                Text(conferenceDays[day].format(), Modifier.padding(10.dp))
            }
        }
    }

    ConferenceDay(conference.presentationsOf(conferenceDay))
}