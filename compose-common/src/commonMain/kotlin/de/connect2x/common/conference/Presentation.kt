package de.connect2x.common.conference

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Checkbox
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import de.connect2x.linuxtage2023.model.Presentation
import de.connect2x.linuxtage2023.model.util.formatTime

@Composable
fun Presentation(presentation: Presentation) {
    val isFavorite = remember { mutableStateOf(presentation.favorite) }
    Column {
        Text(
            "${presentation.time.formatTime()} - ${presentation.until().formatTime()} (${presentation.duration.inWholeMinutes} Minuten)"
        )
        Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween) {
            Text(presentation.title, fontWeight = FontWeight.Bold, modifier = Modifier.weight(1.0f, fill = true))
            Checkbox(checked = isFavorite.value, onCheckedChange = { isFavorite.value = !isFavorite.value })
        }
        Text(presentation.speaker.joinToString { it.fullName })
    }
}