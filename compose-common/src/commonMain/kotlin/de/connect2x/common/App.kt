package de.connect2x.common

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import de.connect2x.common.conference.ConferenceDays
import de.connect2x.linuxtage2023.model.DataSource

@Composable
fun App() {
    val conference = DataSource.linuxTage2023
    val platformName = getPlatformName()

    MaterialTheme {
        Column(Modifier.padding(20.dp)) {
            Text("Client: $platformName", style = MaterialTheme.typography.caption)
            Spacer(Modifier.size(10.dp))
            Text(conference.name, style = MaterialTheme.typography.h3)
            Text(conference.location, style = MaterialTheme.typography.h4)
            Spacer(Modifier.size(20.dp))
            ConferenceDays(conference)
        }
    }
}
