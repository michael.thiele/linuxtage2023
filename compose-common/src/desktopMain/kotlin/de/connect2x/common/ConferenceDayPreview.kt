package de.connect2x.common

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.runtime.Composable
import de.connect2x.common.conference.ConferenceDay
import de.connect2x.linuxtage2023.model.DataSource

@Composable
@Preview
fun ConferenceDayPreview() {
    ConferenceDay(DataSource.linuxTage2023.presentationsOf(DataSource.linuxTage2023.conferenceDays.first()))
}