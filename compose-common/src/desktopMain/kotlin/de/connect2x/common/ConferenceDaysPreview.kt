package de.connect2x.common

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.runtime.Composable
import de.connect2x.common.conference.ConferenceDays
import de.connect2x.linuxtage2023.model.DataSource

@Composable
@Preview
fun ConferenceDaysPreview() {
    ConferenceDays(DataSource.linuxTage2023)
}