package de.connect2x.common

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.runtime.Composable
import de.connect2x.common.conference.Presentation
import de.connect2x.linuxtage2023.model.Speaker
import kotlinx.datetime.LocalDateTime
import kotlin.time.Duration.Companion.minutes

@Composable
@Preview
fun PresentationPreview() {
    Presentation(de.connect2x.linuxtage2023.model.Presentation(
        title = "Ein fiktiver Vortrag über ein wichtiges Thema",
        speaker = listOf(Speaker("Max", "Mustermann")),
        time = LocalDateTime.parse("2023-03-11T09:00:00"),
        duration = 60.minutes,
    ))
}