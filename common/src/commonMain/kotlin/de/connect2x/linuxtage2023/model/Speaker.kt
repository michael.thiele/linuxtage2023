package de.connect2x.linuxtage2023.model

data class Speaker(val firstName: String, val lastName: String) {
    val fullName: String = "$firstName $lastName"
}
