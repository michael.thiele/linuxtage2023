package de.connect2x.linuxtage2023.model

import kotlinx.datetime.LocalDate

data class Conference(val name: String, val location: String, val presentations: List<Presentation>) {
    fun presentationsOf(day: LocalDate): List<Presentation> {
        return presentations.filter { presentation ->
            presentation.time.date == day
        }
            .sortedBy { it.time }
    }

    val conferenceDays: List<LocalDate> =
        presentations.fold(setOf<LocalDate>()) { days, presentation ->
            days + presentation.time.date
        }
            .toList()
            .sorted()
}