package de.connect2x.linuxtage2023.model

import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import kotlinx.datetime.toLocalDateTime
import kotlin.time.Duration

data class Presentation(
    val title: String,
    val speaker: List<Speaker>,
    val time: LocalDateTime,
    val duration: Duration,
    val favorite: Boolean = false,
) {
    fun until(): LocalDateTime {
        return (time.toInstant(TimeZone.UTC) + duration).toLocalDateTime(TimeZone.UTC)
    }

    // for Swift
    val durationInMinutes: Int = duration.inWholeMinutes.toInt()
}
