package de.connect2x.linuxtage2023.model

import kotlinx.datetime.LocalDateTime
import kotlin.time.Duration.Companion.minutes

object DataSource {

    val linuxTage2023 = Conference(
        name = "Linux-Tage 2023",
        location = "Chemnitz",
        presentations = listOf(
            Presentation(
                title = "Kotlin – Java mit Superkräften und Multiplattform",
                speaker = listOf(
                    Speaker("Benedict", "Benken"),
                    Speaker("Michael", "Thiele")
                ),
                time = LocalDateTime.parse("2023-03-11T15:00:00"),
                duration = 60.minutes
            ),
            Presentation(
                title = "Mach es einfach: Nutze Vim!",
                speaker = listOf(Speaker("Marie", "Mann")),
                time = LocalDateTime.parse("2023-03-11T09:00:00"),
                duration = 60.minutes,
            ),
            Presentation(
                title = "Die Ökonomie von Gut und Crypto. Eine Kritik.",
                speaker = listOf(Speaker("Gerrit", "Beine")),
                time = LocalDateTime.parse("2023-03-11T09:00:00"),
                duration = 60.minutes,
            ),
            Presentation(
                title = "Booten mit Feenstaub",
                speaker = listOf(Speaker("André", "Niemann")),
                time = LocalDateTime.parse("2023-03-11T09:00:00"),
                duration = 60.minutes,
            ),
            Presentation(
                title = "Register und Makros in Vim",
                speaker = listOf(Speaker("Jan", "Bundesmann")),
                time = LocalDateTime.parse("2023-03-11T10:00:00"),
                duration = 60.minutes,
            ),
            Presentation(
                title = "Scripting in Go",
                speaker = listOf(Speaker("Jan",  "Delgado")),
                time =  LocalDateTime.parse("2023-03-11T12:00:00"),
                duration = 60.minutes,
            ),
            Presentation(
                title = "Einstieg in die Automatisierung mit Ansible",
                speaker = listOf(Speaker("Jörg", "Kastning")),
                time = LocalDateTime.parse("2023-03-12T10:00:00"),
                duration = 60.minutes,
            ),
            Presentation(
                title = "Konfigurationsmanagement über verschiedene Netze mit AWX",
                speaker = listOf(Speaker("Dr. Ottavia", "Balducci")),
                time = LocalDateTime.parse("2023-03-12T11:00:00"),
                duration = 60.minutes,
            ),
        )
    )
}
