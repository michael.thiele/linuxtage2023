package de.connect2x.linuxtage2023.model.util

import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime

fun LocalDate.format(): String {
    return this.dayOfMonth.toString().padStart(2, '0') +
            "." +
            this.monthNumber.toString().padStart(2, '0') +
            "." +
            this.year
}

fun LocalDateTime.formatTime(): String {
    return this.hour.toString().padStart(2, '0') +
            ":" +
            this.minute.toString().padStart(2, '0')
}
